const prefix = "*";
const Discord = require("discord.js");
const fs = require("fs");
const Client = new Discord.Client({disableEveryone: true});
Client.commands = new Discord.Collection();
//const botsettings = require("./botsettings.json");
var TOKEN = process.env.TOKEN;

fs.readdir("./commands/", (err, files) => {

  if(err) console.log(err);

  let jsfile = files.filter(f => f.split(".").pop() === "js")
  if(jsfile.length <= 0){
    console.log("Couldn't find commands.");
    return;
  }

  jsfile.forEach((f, i) =>{
    let props = require(`./commands/${f}`);
    console.log(`${f} loaded!`);
    Client.commands.set(props.help.name, props);
  });
  
});
  
fs.readdir("./commands/billing-help-commands/", (err, files) => {

  if(err) console.log(err);

  let jsfile = files.filter(f => f.split(".").pop() === "js")
  if(jsfile.length <= 0){
    console.log("Couldn't find commands.");
    return;
  }

  jsfile.forEach((f, i) =>{
    let props = require(`./commands/billing-help-commands/${f}`);
    console.log(`${f} loaded!`);
    Client.commands.set(props.help.name, props);
  });
  
});

fs.readdir("./commands/web-hosting-help/", (err, files) => {

  if(err) console.log(err);

  let jsfile = files.filter(f => f.split(".").pop() === "js")
  if(jsfile.length <= 0){
    console.log("Couldn't find commands.");
    return;
  }

  jsfile.forEach((f, i) =>{
    let props = require(`./commands/web-hosting-help/${f}`);
    console.log(`${f} loaded!`);
    Client.commands.set(props.help.name, props);
  });
  
});

//adawdawdand the number of users?

Client.on("ready", async () => {  
 console.log(`${Client.user.username} is online on ${Client.guilds.size} servers! -=- Made by SpooderCraft || KingOfEnders`);
     try {
       let link = await Client.generateInvite(["ADMINISTRATOR"]);
       console.log(link);
   } catch(e) {
       console.log(e.stack);
   }
    Client.user.setActivity("*help", { type: "WATCHING" });
});

  //.setGame is depreciated now

Client.on("message", async message => {
  if(message.author.bot) return;
  if(message.channel.type === "dm") return;
  if(message.content.startsWith(prefix) && Client.user.presence.status === "dnd"){
     if(message.author.id !== "131417543888863232")return message.channel.send("**Hey, `" + message.author.username + "` The Bot Is Currently Under `Maintenance/Developing Mode`**").then(msg => {msg.delete(4000)});
     
Client.on("guildMemberAdd", async member => {
  

  let welcomechannel = member.guild.channels.find(`name`, "welcome");
  welcomechannel.send(`Welcome ${member}!`);
});



Client.on("guildMemberRemove", async member => {
  


  let welcomechannel = member.guild.channels.find(`name`, "welcome");
  welcomechannel.send(`Cya ${member}`);

})

}
    
exports.conf = {
    enabled: true,
    ignoreBots: false,
    ignoreSelf: false,
};
  
if(message.content == "<@446430324503740446>"){
        message.channel.send({embed:{
                color: 0x00FF74,
                description: "Hello, " + message.member.displayName + " \n\My name is <@446430324503740446> and i work for beastnode! I was made by <@131417543888863232> and <@412374698987487242>! \n\I was made to help you and everyone here! \n\If you need help please do `*help` and i will help you! \n\Thank you! <3"

            }
        });
}
      
  if(!message.content.startsWith(prefix))return;

  let messageArray = message.content.split(" ");
  let cmd = messageArray[0];
  let args = messageArray.slice(1);

  let commandfile = Client.commands.get(cmd.slice(prefix.length));
  if(commandfile) commandfile.run(Client,message,args);
  
});  

Client.login(process.env.token);
