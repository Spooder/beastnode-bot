const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => { 
    let bUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!bUser) return message.channel.send("**:warning: Hey, `" + message.author.username + "` You Need To `MENTION USER` Please Try Again :warning:**");
    let bReason = args.join(" ").slice(22);
    if(!message.member.hasPermission("BAN_MEMBERS")) return message.channel.send("**:warning: Hey, `" + message.author.username + "` You do not have the perm `BAN_MEMBERS` :warning:**");
    if(bUser.hasPermission("ADMINISTRATOR")) return message.channel.send("**:warning: `" + message.author.username + "`! You Can't Kick A Member That Has A The Perm `ADMINISTRATOR` As You :warning:**");
    
    let banEmbed = new Discord.RichEmbed()
    .setDescription("~Ban~")
    .setColor("#bc0000")
    .addField("Banned User", `${bUser} with ID ${bUser.id}`)
    .addField("Banned By", `<@${message.author.id}> with ID ${message.author.id}`)
    .addField("Banned In", message.channel)
    .addField("Time", message.createdAt)
    .addField("Reason", bReason);

    let incidentchannel = message.guild.channels.find(`name`, "logs");
    if(!incidentchannel) return message.channel.send("Can't find incidents channel.");
}

module.exports.help = {
  name:"ban"
}
