const Discord = require("discord.js");
const urban = require("urban");

module.exports.run = async (client,message,args) => {
    if(message.channel.name !== "nsfw") return message.channel.send("**Hey, `" + message.author.username + "` That Command Is Only For nsfw**") 
    urban.random().first(json => {
        let embed = new Discord.RichEmbed()
        .setTitle(json.word)
        .setDescription(json.definition)
        .addField("Upvotes", json.thumbs_up, true)
        .addField("Downvotes", json.thumbs_down, true)
        .setFooter(`Written by ${json.author}`);

        message.channel.send({embed})
    })


}
module.exports.help = {
    name: "randomUrban"
}