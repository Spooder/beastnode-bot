const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("Prefix", "**`!`**")
        .addField("**    **","**       **")
        .addField("Blilling Commands:", "Remeber you can do `!billing-help-(nameofcommand)` to see what it is so you do not need to look at the site! The Command will be on the left side so `!billing-help-ebft` would be it!")
        .addField("** **","**    **")
        .addField("**Example Command:**","**    **")
        .addField("**ebft**","[Enjin/Buycraft Free Trials](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=166)")
        .addField("** **","**    **")
        .addField("** **","**`Help Commands`**")
        .addField("**ac/fa**","[Adding credit/funds to your account](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=121)")
        .addField("ab","[Automatic Billing](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=123)")
        .addField("ap","[BeastNode Affiliate Porgram](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=149)")
        .addField("bsu","[Billing Sub-User](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=120)")
        .addField("ebft","[Enjin/Buycraft Free Trials](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=166)")
        .addField("pas","[How do i purchase additional servers](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=1)")
        .addField("cys","[How to cancel your survices](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=47)")
        .addField("ps/pc","[Paypal Subcription/Ruccurting Payments cancellation](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=29)")
        .addField("urs","[Upgrading your service](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=42)")
        .addField("ads","[Additional Disk Space](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=151)")
        .addField("** **","**    **")
        .addField("Also","If none of these can not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});
}

module.exports.help = {
  name:"billing-help"
}
