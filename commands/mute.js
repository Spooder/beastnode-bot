const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
        if (!message.member.permissions.has("MANAGE_MESSAGES")) return message.channel.send("**Hey, `" + message.member.displayName + "` You Do Not Have Enough Perms To Mute or You Need The Perm `MANAGE_MESSAGE`!**");


        let toMute = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);
        if(!toMute) return message.channel.send("**Hey, `" + message.member.displayName + "` You Need To `MENTION USER` Please Try Again**");

        if(toMute.id === message.author.id) return message.channel.send("**Hey, `" + message.member.displayName + "` You Can't Mute Yourself**");
        if(toMute.highestRole.position >= message.member.highestRole.position) return message.channel.send("**Hey, `" + message.member.displayName + "` You Can't Mute A Member That Has A `Better/Higher` Role As You**");

        let role = message.guild.roles.find(r => r.name === "Muted");
        if(!role) {
            try{
               role = await message.guild.createRole({
                 name: "Muted",
                 color: "#000000",
                preissions: []
              });

              message.guild.channels.forEach(async (channel, id) => {
                  await channel.overwritePermissions(role, {
                      SEND_MESSAGES: false,
                      ADD_REACTIONS: false
                  });
              });
          } catch(e) {
              console.log(e.stack);
          }
      }
       
          if(toMute.roles.has(role.id)) return message.channel.send("**Hey, `" + message.member.displayName + "` This User Is `ALREADY` Muted**");

          await toMute.addRole(role);
          message.channel.send("**`" + message.member.displayName + "` The Deed is done!**");
          
    let banEmbed = new Discord.RichEmbed()
    .setDescription("~Muted~")
    .setColor("#bc0000")
    .addField("Muted User", `${bUser} with ID ${bUser.id}`)
    .addField("Muted By", `<@${message.author.id}> with ID ${message.author.id}`)
    .addField("Muted In", message.channel)
    .addField("Time", message.createdAt)
    .addField("Reason", bReason);

    let incidentchannel = message.guild.channels.find(`name`, "logs");
    if(!incidentchannel) return message.channel.send("Can't find incidents channel.");

    message.guild.member(bUser).ban(bReason);
    incidentchannel.send(banEmbed);
}

module.exports.help = {
  name:"mute"
}
