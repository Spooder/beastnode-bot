const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

  //!addrole @andrew Dog Person
  if(!message.member.hasPermission("MANAGE_MEMBERS")) return message.channel.send("**Hey, `" + message.author.username + "` You Do Not Have Enough Perms To Mute or You Need `Manage Messages/Staff Team Role`**");
  let rMember = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);
  if(!rMember) return message.channel.send("**Hey, `" + message.author.username + "` I Couldn't Find That Member Please Try Again**");
  let role = args.join(" ").slice(22);
  if(!role) return message.channel.send("**Hey, `" + message.author.username + "` You Didn't Pick A Role Name Please Try Again**");
  let gRole = message.guild.roles.find(`name`, role);
  if(!gRole) return message.channel.send("**Hey, `" + message.author.username + "` I Couldn't Find That Role Please Try Again**");

  if(rMember.roles.has(gRole.id)) return message.channel.send("**Hey, `" + message.author.username + "` They Already Have That Role**");
  await(rMember.addRole(gRole.id));

  try{
    await rMember.send(`**Congrats, You Have Been Given The Role ${gRole.name}**`)
  }catch(e){
    console.log(e.stack);
    message.channel.send(`**Congrats To <@${rMember.id}>, They Have Been Given The Role ${gRole.name}. We Tried To DM Them, But Their DMs Are Locked.**`)
  }
}

module.exports.help = {
  name: "addrole"
}