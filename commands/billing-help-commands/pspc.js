const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- ps/pc")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Paypal Subscription/Recurring Payments Cancellation","When you cancel our service and you have a Paypal subscription or automatic payment setup, you will have to manually cancel it from your Paypal account. Please follow these steps to do so:")
        .addField("Older/Classic PayPal website layout:","Log in to your PayPal account. \n\Click Profile at the top of the page. \n\Click My Money, then click Update beside My preapproved payments to find your payment. \n\Select the payment, and then click Cancel.")
        .addField("New PayPal website layout:","Log into your PayPal account. \n\Click the Settings icon in the top right of the page `gear symbol` \n\Click the Payments tab at the top of the page \n\Click the Manage Pre-Approved Payments option \n\Select the payment for BeastNode under the Merchant column, and then click Cancel.")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(15000)});

}

module.exports.help = {
  name:"billing-help-ps/pc"
}
