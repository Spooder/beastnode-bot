const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- ap")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("BeastNode Affiliate Program","Looking to earn money? Then look no more as the BeastNode affiliate program is one of the easiest ways to earn an income to help support your own Minecraft server!")
        .addField("All it takes to get started is just a few simple steps.","1. Login to your billing portal at https://www.beastnode.com/portal \n\2. Click on the `Affiliates` link in the billing navigation bar \n\3. Click `Activate Affiliate Account` \n\4. You'll now get a URL that you can give out or post to start making money! \n\n\Whenever someone uses that URL to go to our site and they make a purchase on any product, you will get 5% of the value of their server as income into your affiliate account. You will receive this income every time they make a payment for their server! \n\n\Whenever someone uses that URL to go to our site and they make a purchase on any product, you will get 5% of the value of their server as income into your affiliate account. You will receive this income every time they make a payment for their server!")
        .addField("With this withdrawal you will have two options:","1. Have the balance added as credit to your account \n\2. Have it sent to a Paypal account `please note Paypal will charge a small percentage for transaction fees on the balance that is sent`")
        .addField("** **","`Warning:` The page text was to long to register [Click Here]9https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=149) To read the rest of the info!")
        
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});

}

module.exports.help = {
  name:"billing-help-ap"
}
