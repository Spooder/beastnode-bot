const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- bsu")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Billing Sub-User","With our billing system, you will be able to assign sub-users to your account to manage certain billing properties. To do so, follow these steps: \n\n\1. Login to the billing portal at https://www.beastnode.com/portal \n\2. Hover over the `Account` navigation option and select `Contacts/Sub Accounts` \n\3. Once in the sub accounts page, simply fill in the user details as desired and make sure to check the `Tick to configure as a sub-account with client area access` to allow them access to the billing features. \n\4. After you check that box, you should see a list of checkboxes come up that allows you to choose what part of the billing system you want to give them access to `such as the ability to view and pay invoices, send support tickets, etc`.")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});

}

module.exports.help = {
  name:"billing-help-bsu"
}
