const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- ac/fa")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("Adding Credit/Funds To Your Account","To add credit/funds to your account, please send a ticket to https://www.beastnode.com/r/billing to request how much credit you want added and we'll issue you the invoice accordingly for it.")
        .addField("**    **","**       **")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")

    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});


}

module.exports.help = {
  name:"billing-help-ac/fa"
}
