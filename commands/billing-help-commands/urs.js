const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- urs")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Upgrading Your Service","We allow for service upgrades/downgrades at anytime. \n\n\For all Multicraft based Minecraft servers, upgrades can be done automatically by following these steps: \n\n\1. Login to the billing panel at: https://www.beastnode.com/portal \n\2. Navigate to Account > My Services \n\3. Click the down arrow next to your desired server, then click to Upgrade/Downgrade. `If the request is mid-billing cycle, you will be sent a prorated invoice for the upgrade.` \n\n\Upgrading will NOT affect your server data in any way, so your world, plugins, mods, etc, will still be there. Don't forget to reboot your server after the upgrade is processed so the changes can take place.")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});

}

module.exports.help = {
  name:"billing-help-urs"
}
