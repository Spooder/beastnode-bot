const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- cys")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("How to Cancel Your Services","Login to your billing portal: https://www.beastnode.com/portal/clientarea.php \n\Click the **Services** box to bring up a list of your services. \n\On this page, click on the service you wish to cancel. \n\Finally, click on the red **Request Cancellation** box. \n\ You can select to either schedule the cancellation immediately or on the last day of your service `recommended` \n\Make sure to cancel your Paypal subscription/automatic payments if you have any.")
        .addField("Please Note:","Once the service is cancelled it will be removed from the system, files included. If you need to download your server files please make sure to do so before the cancellation runs - once it has ran the files cannot be recovered and the service cannot be reactivated. \n\If you delete your files its not are fault.")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(10000)});

}

module.exports.help = {
  name:"billing-help-cys"
}
