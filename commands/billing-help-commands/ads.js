const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- ads")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Additional Disk Space","For those on our premium level Minecraft hosting packages, SSD disk space starts with a limit of 5GB. Additional disk space can be requested via a support ticket with appropriate justification and if within reason. Data such as backups, general file storage, logging plugins (i.e CoreProtect), map plugins (i.e. Dynmap), and various other plugins/mods that use large amounts of disk space are not justifiable and as such will require additional fees for more space. If the additional disk space is justified, additional space can be requested via support ticket in increments of 5GB when the server is within 1GB of its current disk space limit. If a specific node does not have enough space to support your request, a node change may be required, which will change your server's IP.")
        .addField("`Warning`!","The Page was to large to load all info! Please [Click Here](https://www.beastnode.com/portal/knowledgebase/151/Additional-Disk-Space.html) to read the page!")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(15000)});

}

module.exports.help = {
  name:"billing-help-ads"
}
