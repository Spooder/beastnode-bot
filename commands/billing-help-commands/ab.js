const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands -=- ab")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Automatic Billing","You have the option of automatic billing using both of our payment methods (Paypal & credit card). \n\n\For credit cards, auto billing is enabled by default for customer convenience. The automatic billing is terminated when you cancel your service by following this guide: https://www.beastnode.com/portal/knowledgebase/47/How-to-Cancel-Your-Services.html \n\n\`Warning:` The page was to long to register! Please [Click here](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=123) To read the side info!")
        
        
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});

}

module.exports.help = {
  name:"billing-help-ab"
}
//adadawd
