const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
        message.guild.roles.find("name", "Staff"); 
          return message.channel.send("**Hey, `" + message.author.username + "` You Do Not Have Enough Perms To UnMute or You Need `Staff` Role**");

        let toMute = message.guild.member(message.mentions.users.first()) || message.guild.member(args[0]);
        if(!toMute) return message.channel.send("**Hey, `" + message.author.username + "` You Need To `MENTION USER` Please Try Again**")

          let role = message.guild.roles.find(r => r.name === "Muted");

          if(!role || !toMute.roles.has(role.id)) return message.channel.send("**Hey, `" + message.author.username + "` This User Is `NOT` Muted**");

          await toMute.removeRole(role);
          let embed = new Discord.RichEmbed()
          message.channel.send("**Hey, `" + message.author.username + "` I Have `UNMUTED` Them**");
}

module.exports.help = {
  name:"unmute"
}