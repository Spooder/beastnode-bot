const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("WebHosting Help Commands -=- ipr")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Create a DNS / IP Server Redirect For Your Minecraft Server in cPanel","If you have a web hosting plan from BeastNode, you can setup your own URL style redirect to your Minecraft server (i.e. play.yourdomain.com). You can also follow these steps if you have another web host that uses cPanel.")
        .addField("Creating an `A` name record:","1. First, login to your cPanel account. Once in, look for `Advanced Zone Editor` and click on it. \n\2. You'll need to create an `A` name record for this, so make sure `Type` is set to `A` \n\3. Fill in the `Name` field with your desired subdomain `i.e. play.yourdomain.com - the **play** part can be anything you want` \n\4. Set `TTL` to 3600. \n\5. In the `Address` box simply enter your server's IP without the port number. \n\6. Click Add Record to submit the changes and create your subdomain/domain IP/server redirect. Please allow up to 24 hours for the redirect to propagate fully around the internet. \n\n\If your server has the 25565 port (dedicated IP) assigned to it, then you can stop here. If you have any other port number, please proceed to the following steps. \n\Without a dedicated IP with the 25565 port, you'll need to create an `SRV` record in addition to the `A` record above.")
        .addField("Creating an `SRV` record:","1. Go back into the `Advanced Zone Editor` page \n\2. Set the `Type` to `SRV` \n\3. Set the `Name` to the following: _minecraft._tcp.play.yourdomain.com Then replace `play.yourdomain.com` with whatever you set your `A` record to as above \n\4. Set `TTL` to 3600 \n\5. Set `Priority` and `Weight` to 0 \n\6. Set `Port` to your server's port number \n\7. Set `Target` to whatever your `A` record was set to in the above section \n\8. Click Add Record to submit the changes and create your SRV record. Please allow up to 24 hours for the redirect to propagate fully around the internet.")
        .addField("Images","[A Name Help](https://www.beastnode.com/images/kb/cpanel/a-record.png) \n\[SRV Help](https://www.beastnode.com/images/kb/cpanel/srv-record.png)")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(25000)});

}

module.exports.help = {
  name:"webhosting-help-ipr"
}
