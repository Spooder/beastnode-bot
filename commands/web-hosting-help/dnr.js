const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("WebHosting Help Commands -=- dnr")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Domain Name Registration and Setup","Before you purchase any form of web hosting, you first need a domain name to direct to your web host. In simple terms, the domain name is the address you type into your browser's URL bar to access your server `ex: mydomain.com`. Domains are actually separate from the web hosting itself (the service that actually contains your website, data, etc). As we are not a domain registrar, you will need to purchase your domain name from a separate company. There are many domain registrars on the internet, but the two largest are [Namecheap](www.namecheap.com) and [GoDaddy](www.GoDaddy.com). Domains are registered in yearly increments at around $10/year `depending on the type of domain`. \n\n\Once you have found and registered the domain you want with a domain registrar, you can now purchase web hosting from our website. During the ordering process you'll be presented with a domain entry page - this is where you'll enter the domain name that you just purchased. Once everything is completed, you simply have to change your domain's nameservers to the ones we provide to you in the web hosting activation email. You can refer to these guides to setup the nameservers depending on which registrar you used: \n\https://www.namecheap.com/support/knowledgebase/article.aspx/767/10/how-can-i-change-the-nameservers-for-my-domain \n\https://www.godaddy.com/help/setting-custom-nameservers-for-domains-registered-with-us-12317")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(30000)});

}

module.exports.help = {
  name:"webhosting-help-dnr"
}