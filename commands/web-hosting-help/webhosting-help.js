
const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Billing Help Commands")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("Prefix", "**`!`**")
        .addField("**    **","**       **")
        .addField("Blilling Commands:", "Remeber you can do `!billing-help-(nameofcommand)` to see what it is so you do not need to look at the site! The Command will be on the left side so `!billing-help-ebft` would be it!")
        .addField("** **","**    **")
        .addField("**Example Command:**","**    **")
        .addField("**ebft**","[Enjin/Buycraft Free Trials](https://www.beastnode.com/portal/knowledgebase.php?action=displayarticle&id=166)")
        .addField("** **","**    **")
        .addField("** **","**`Help Commands`**")
        .addField("ssh","[Access SSH on Your Website](https://www.beastnode.com/portal/knowledgebase/170/Access-SSH-on-Your-Website.html)")
        .addField("ipr","[Create a DNS / IP Server Redirect For Your Minecraft Server in cPanel](Create a DNS / IP Server Redirect For Your Minecraft Server in cPanel)")
        .addField("dnr","[Domain Name Registration and Setup](https://www.beastnode.com/portal/knowledgebase/135/Domain-Name-Registration-and-Setup.html)")
        .addField("mysql","[Getting Started / Create a MySQL Database](https://www.beastnode.com/portal/knowledgebase/64/Getting-Started-or-Create-a-MySQL-Database.html)")
        .addField("aname","[How To Add A Name Records To Your Domain](https://www.beastnode.com/portal/knowledgebase/147/How-To-Add-A-Name-Records-to-Your-Domain.html)")
        .addField("exmysql","[Web Hosting External MySQL Connections](https://www.beastnode.com/portal/knowledgebase/57/Web-Hosting-External-MySQL-Connections.html)")
        .addField("dwh","[What is a Domain and Web Hosting?](What is a Domain and Web Hosting?)")
        .addField("** **","**    **")
        .addField("Also","If none of these can not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(16000)});
}

module.exports.help = {
  name:"webhosting-help"
}
