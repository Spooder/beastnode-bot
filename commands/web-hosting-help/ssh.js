const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("WebHosting Help Commands -=- ssh")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`Thank you for useing Beastnode as your host!`, `${message.guild.iconURL}`)
        .addField("**    **","**       **")
        .addField("Access SSH on Your Website","To access SSH on your website with our web hosting plans all you have to do is use the IP address provided to you in the web hosting welcome email, then enter in port 21098. \n\n\From there, you'll get a login page asking you for your cPanel username `hit enter once you enter it`. Then enter your cPanel user password when asked for it, then hit enter to login.")
        .addField("Also","If this does not help you please make a ticket on the site. \n\Ticket create: [Click here](https://www.beastnode.com/portal/submitticket.php) \n\or \n\ask for help in `#community-support`")
        .addField("** **","**      **")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(8000)});

}

module.exports.help = {
  name:"webhosting-help-ssh"
}
