const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("Help Commands")
        .setDescription("**  **")
        .setColor("#008080")
        .setThumbnail(bicon)
        .setFooter(`${message.guild.name} | ${message.guild.memberCount} Members | Made By KingOfEnders and SpooderCraft`, `${message.guild.iconURL}`)
        .addField("Prefix", "**`!`**")
        .addField("**    **","**       **")
        .addField("-=- BEFORE YOU ASK FOR SUPPORT FROM STAFF -=-", "You might want to look at commands to see if I can help!")
        .addField("** **","**    **")
        .addField("Support Commands", "**billing-help** `-=-` **vps-help** `-=-` **webhosting-help** `-=-` **mumble-help** `-=-` **minecraft-help** `-=-` **support-help**")
        .addField("**   **","**    **")   
        .addField("BotOwner", "**`setinv` <-> `seton` <-> `setdnd` <-> `setidle` <-> `eval` <-> `restart`**")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});

}

module.exports.help = {
  name:"help"
}
