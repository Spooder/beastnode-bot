const Discord = require("discord.js");

module.exports.run = async (Client, message, args) => {
    let bicon = Client.user.displayAvatarURL;
    let botembed = new Discord.RichEmbed()
    .setDescription("Bot Information")
    .setColor("#15f153")
    .setThumbnail(bicon)
    .addField("Bot Name", Client.user.username)
    .addField("Created On", Client.user.createdAt);

    message.channel.send(botembed);
}

module.exports.help = {
  name:"botinfo"
}